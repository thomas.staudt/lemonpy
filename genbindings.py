import sys, os, re

class Base:

    def read_template(self, name):
        fname = "templates/" + name + ".cc"
        with open(fname, "r") as file:
            return file.read()

    def varname(self, base):
        if base in self.vars.keys():
            i = self.vars[base]
            self.vars[base] += 1
            return base + str(i)
        else:
            self.vars[base] = 1
            return base + str(0)

    def binding_name(self):
        return re.sub(r'(?<!^)(?=[A-Z])', '_', self.args["G"]).lower()

    def start_binding(self):
        return "void {}(py::module_ & m) {{".format(self.binding_name())

    def end_binding(self):
        return "}"

    def indent(self, text):
        return "\n".join(["  " + line for line in text.split("\n")])


class Api(Base):

    base_size = 0

    def __init__(self, base_size):
        self.base_size = base_size

    def generate_bindings(self):
        code = self.read_template("api").format(S = self.base_size)
        with open("src/api" + str(self.base_size) + ".cc", "w") as file:
            file.write(code)


class Digraph(Base):

    def digraph_common(self):
        return ("digraph_common", {**self.args})

    def network_simplex(self):
        args = {}
        args["V"] = self.varname("solver")
        args["S"] = "NetworkSimplex<{G}, {T}>".format(**self.args)
        return ("network_simplex", {**self.args, **args})

    def cost_scaling(self):
        args = {}
        args["V"] = self.varname("solver")
        args["S"] = "CostScaling<{G}, {T}>".format(**self.args)
        return ("cost_scaling", {**self.args, **args})

    def templates(self):
        return [
            self.digraph_common(),
            self.network_simplex(),
            self.cost_scaling()
        ]

    def __init__(self, graph):
        self.args = {}
        self.vars = {}
        self.args["G"] = graph
        self.args["T"] = "INTEGER_TYPE"

    def generate_bindings(self):
        target = self.binding_name()
        lines = []

        lines.append(self.read_template("header"))
        lines.append(self.start_binding())
        lines.append(self.indent(self.read_template(target)))

        for (t, args) in self.templates():
            lines.append(self.indent(self.read_template(t).format(**args)))

        lines.append(self.end_binding())

        with open("src/" + target + ".cc", "w") as file:
            file.write("\n".join(lines))


if __name__ == "__main__":
    bindings = [
        Api(32),
        Api(64),
        Digraph("ListDigraph"),
        Digraph("SmartDigraph")
    ]
    for b in bindings:
        b.generate_bindings()

