from setuptools import setup, find_packages

# Available at setup time due to pyproject.toml
from pybind11.setup_helpers import Pybind11Extension, ParallelCompile, build_ext
from pybind11 import get_cmake_dir

import sys, re
from glob import glob

__version__ = "0.0.1"


ParallelCompile("NPY_NUM_BUILD_JOBS").install()

ext_modules = [
    Pybind11Extension("lemonpy.api32",
        [f for f in sorted(glob("src/*.cc")) if not re.search('/api64', f)],
        define_macros = [('LEMONPY_BASE_SIZE', '32')],
        libraries = ["emon"],
        cxx_std=14
        ),
    Pybind11Extension("lemonpy.api64",
        [f for f in sorted(glob("src/*.cc")) if not re.search('/api32', f)],
        define_macros = [('LEMONPY_BASE_SIZE', '64')],
        libraries = ["emon"],
        cxx_std=14
        ),
]

setup(
    name="lemonpy",
    version=__version__,
    author="Thomas Staudt",
    author_email="tscode@posteo.net",
    url="todo",
    description="Bindings to the c++ based lemon graph library",
    long_description="",

    ext_modules=ext_modules,
    extras_require={"test": "pytest"},
    #package_dir={"": "lemonpy"},
    packages=["lemonpy"],
    python_requires=">=3.6",
    # Currently, build_ext only provides an optional "highest supported C++
    # level" feature, but in the future it may provide more features.
    cmdclass={"build_ext": build_ext},
    zip_safe=False,
)
