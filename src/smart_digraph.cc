// This file was auto-generated by genbindings.py

#include <lemon/list_graph.h>
#include <lemon/smart_graph.h>
#include <lemon/static_graph.h>

#include <lemon/network_simplex.h>
#include <lemon/cost_scaling.h>
#include <lemon/capacity_scaling.h>
#include <lemon/cycle_canceling.h>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

using namespace lemon;
using namespace std;

#if LEMONPY_BASE_SIZE == 64
#define INTEGER_TYPE long long int
#else
#define INTEGER_TYPE int
#endif


void smart_digraph(py::module_ & m) {
  
  py::class_<SmartDigraph> graph(m, "SmartDigraph");
  
  
  graph
    .def(py::init<>())
    .def("addNode", &SmartDigraph::addNode, "Add a new node to the digraph.")
    .def("addArc", &SmartDigraph::addArc, "Add a new arc to the digraph.")
    .def("valid", py::overload_cast<SmartDigraph::Node>(&SmartDigraph::valid, py::const_), "Node validity check.")
    .def("valid", py::overload_cast<SmartDigraph::Arc>(&SmartDigraph::valid, py::const_), "Arc validity check.")
    .def("split", &SmartDigraph::split, "Split a node.")
    .def("clear", &SmartDigraph::clear, "Clear the digraph.")
    .def("reserveNode", &SmartDigraph::reserveNode, "Reserve memory for nodes.")
    .def("reserveArc", &SmartDigraph::reserveArc, "Reserve memory for arcs.");
  
  
  
  graph
    .def("id", py::overload_cast<SmartDigraph::Node>(&SmartDigraph::id), "The ID of the node.")
    .def("id", py::overload_cast<SmartDigraph::Arc>(&SmartDigraph::id), "The ID of the arc.")
    .def("nodeFromId", &SmartDigraph::nodeFromId, "The node with the given ID.")
    .def("arcFromId", &SmartDigraph::arcFromId, "The arc with the given ID.")
    .def("oppositeNode", &SmartDigraph::oppositeNode, "The opposite node on the arc.")
    .def("source", &SmartDigraph::source, "The source node of the arc.")
    .def("target", &SmartDigraph::target, "The target node of the arc.");
  
  
  py::class_<SmartDigraph::Node>(graph, "Node");
  py::class_<SmartDigraph::Arc>(graph, "Arc");
  
  
  py::class_<SmartDigraph::NodeMap<INTEGER_TYPE>>(graph, "NodeMap")
    .def(py::init<const SmartDigraph &>())
    .def("set", &SmartDigraph::NodeMap<INTEGER_TYPE>::set)
    .def("__setitem__", &SmartDigraph::NodeMap<INTEGER_TYPE>::set)
    .def("get", py::overload_cast<const SmartDigraph::Node &>(&SmartDigraph::NodeMap<INTEGER_TYPE>::operator[], py::const_))
    .def("__getitem__", py::overload_cast<const SmartDigraph::Node &>(&SmartDigraph::NodeMap<INTEGER_TYPE>::operator[], py::const_));
  
  
  py::class_<SmartDigraph::ArcMap<INTEGER_TYPE>>(graph, "ArcMap")
    .def(py::init<const SmartDigraph &>())
    .def("set", &SmartDigraph::ArcMap<INTEGER_TYPE>::set)
    .def("__setitem__", &SmartDigraph::ArcMap<INTEGER_TYPE>::set)
    .def("get", py::overload_cast<const SmartDigraph::Arc &>(&SmartDigraph::ArcMap<INTEGER_TYPE>::operator[], py::const_))
    .def("__getitem__", py::overload_cast<const SmartDigraph::Arc &>(&SmartDigraph::ArcMap<INTEGER_TYPE>::operator[], py::const_));
  
  
  py::class_<NetworkSimplex<SmartDigraph, INTEGER_TYPE>> solver0(graph, "NetworkSimplex");
  solver0
    .def(py::init<const SmartDigraph &>())
    // setup
    .def("lowerMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::lowerMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the lower bounds on the arcs.")
    .def("upperMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::upperMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the upper bounds (capacities) on the arcs.")
    .def("costMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::costMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the costs of the arcs.")
    .def("supplyMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::supplyMap<SmartDigraph::NodeMap<INTEGER_TYPE>>, "Set the supply values of the nodes.")
    // running and resetting
    .def("run", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::run, "Run the algorithm.")
    .def("resetParams", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::resetParams, "Reset all the parameters that have been given before.")
    .def("reset", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::reset, "Reset the internal data structures and all the parameters that have been given before.")
    // query results
    .def("totalCost", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::totalCost<double>, "Return the total cost of the found flow.")
    .def("flow", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::flow, "Return the flow on the given arc.")
    .def("potential", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::potential, "Return the flow map (the primal solution).")
    .def("flowMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::flowMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Return the potential (dual value) of the given node.")
    .def("potentialMap", &NetworkSimplex<SmartDigraph, INTEGER_TYPE>::potentialMap<SmartDigraph::NodeMap<INTEGER_TYPE>>, "Return the potential map (the dual solution).");
      
  py::enum_<NetworkSimplex<SmartDigraph, INTEGER_TYPE>::ProblemType>(solver0, "ProblemType")
    .value("INFEASIBLE", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::ProblemType::INFEASIBLE)
    .value("OPTIMAL", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::ProblemType::OPTIMAL)
    .value("UNBOUNDED", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::ProblemType::UNBOUNDED);
  
  py::enum_<NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule>(solver0, "PivotRule")
    .value("FIRST_ELIGIBLE", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule::FIRST_ELIGIBLE)
    .value("BEST_ELIGIBLE", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule::BEST_ELIGIBLE)
    .value("BLOCK_SEARCH", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule::BLOCK_SEARCH)
    .value("CANDIDATE_LIST", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule::CANDIDATE_LIST)
    .value("ALTERING_LIST", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::PivotRule::ALTERING_LIST);
  
  py::enum_<NetworkSimplex<SmartDigraph, INTEGER_TYPE>::SupplyType> (solver0, "SupplyType")
    .value("GEQ", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::SupplyType::GEQ)
    .value("LEQ", NetworkSimplex<SmartDigraph, INTEGER_TYPE>::SupplyType::LEQ);
  
  
  py::class_<CostScaling<SmartDigraph, INTEGER_TYPE>> solver1(graph, "CostScaling");
  solver1
    .def(py::init<const SmartDigraph &>())
    // setup
    .def("lowerMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::lowerMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the lower bounds on the arcs.")
    .def("upperMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::upperMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the upper bounds (capacities) on the arcs.")
    .def("costMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::costMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Set the costs of the arcs.")
    .def("supplyMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::supplyMap<SmartDigraph::NodeMap<INTEGER_TYPE>>, "Set the supply values of the nodes.")
    // running and resetting
    .def("run", &CostScaling<SmartDigraph, INTEGER_TYPE>::run, "Run the algorithm.")
    .def("resetParams", &CostScaling<SmartDigraph, INTEGER_TYPE>::resetParams, "Reset all the parameters that have been given before.")
    .def("reset", &CostScaling<SmartDigraph, INTEGER_TYPE>::reset, "Reset the internal data structures and all the parameters that have been given before.")
    // query results
    .def("totalCost", &CostScaling<SmartDigraph, INTEGER_TYPE>::totalCost<double>, "Return the total cost of the found flow.")
    .def("flow", &CostScaling<SmartDigraph, INTEGER_TYPE>::flow, "Return the flow on the given arc.")
    .def("potential", &CostScaling<SmartDigraph, INTEGER_TYPE>::potential, "Return the flow map (the primal solution).")
    .def("flowMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::flowMap<SmartDigraph::ArcMap<INTEGER_TYPE>>, "Return the potential (dual value) of the given node.")
    .def("potentialMap", &CostScaling<SmartDigraph, INTEGER_TYPE>::potentialMap<SmartDigraph::NodeMap<INTEGER_TYPE>>, "Return the potential map (the dual solution).");
  
  py::enum_<CostScaling<SmartDigraph, INTEGER_TYPE>::ProblemType>(solver1, "ProblemType")
    .value("INFEASIBLE", CostScaling<SmartDigraph, INTEGER_TYPE>::ProblemType::INFEASIBLE)
    .value("OPTIMAL", CostScaling<SmartDigraph, INTEGER_TYPE>::ProblemType::OPTIMAL)
    .value("UNBOUNDED", CostScaling<SmartDigraph, INTEGER_TYPE>::ProblemType::UNBOUNDED);
  
  py::enum_<CostScaling<SmartDigraph, INTEGER_TYPE>::Method>(solver1, "Method")
    .value("PUSH", CostScaling<SmartDigraph, INTEGER_TYPE>::Method::PUSH)
    .value("AUGMENT", CostScaling<SmartDigraph, INTEGER_TYPE>::Method::AUGMENT)
    .value("PARTIAL_AUGMENT", CostScaling<SmartDigraph, INTEGER_TYPE>::Method::PARTIAL_AUGMENT);
  
}