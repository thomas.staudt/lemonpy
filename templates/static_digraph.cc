
StaticDigraph * create_static_digraph( int n,
                                       py::array_t<int> sources,
                                       py::array_t<int> targets ) {{

  py::size_t n_arcs = py::len(sources);
  if (n_arcs != py::len(targets))
    throw invalid_argument("source and target node id arrays must have same length");

  auto s = sources.unchecked<1>();
  auto t = targets.unchecked<1>();
  vector<pair<int, int>> pairs;
  int prev_s = 0;
  for (py::size_t i = 0; i < n_arcs; i++) {{
    int si = s(i);
    int ti = t(i);
    // consistency checks
    if (si < prev_s)
      throw std::invalid_argument("source nodes must be ordered");
    if (si < 0 || ti < 0 || si >= n || ti >= n)
      throw std::out_of_range("node out of range");
    // fill containers
    pair<int, int> p(si, ti);
    pairs.push_back(p);
    prev_s = si;
  }}

  // construct the static graph
  StaticDigraph g = new StaticDigraph();
  g.build(n, pairs.begin(), pairs.end());

  return g;

}}

py::class_<StaticDigraph> graph(m, "StaticDigraph");

graph
  .def(py::init<>())
  .def(py::init<int, py::array_t<int>, py::array_t<int>>(&create_static_digraph));


