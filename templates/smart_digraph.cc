
py::class_<SmartDigraph> graph(m, "SmartDigraph");


graph
  .def(py::init<>())
  .def("addNode", &SmartDigraph::addNode, "Add a new node to the digraph.")
  .def("addArc", &SmartDigraph::addArc, "Add a new arc to the digraph.")
  .def("valid", py::overload_cast<SmartDigraph::Node>(&SmartDigraph::valid, py::const_), "Node validity check.")
  .def("valid", py::overload_cast<SmartDigraph::Arc>(&SmartDigraph::valid, py::const_), "Arc validity check.")
  .def("split", &SmartDigraph::split, "Split a node.")
  .def("clear", &SmartDigraph::clear, "Clear the digraph.")
  .def("reserveNode", &SmartDigraph::reserveNode, "Reserve memory for nodes.")
  .def("reserveArc", &SmartDigraph::reserveArc, "Reserve memory for arcs.");

