
py::class_<{S}> {V}(graph, "CostScaling");
{V}
  .def(py::init<const {G} &>())
  // setup
  .def("lowerMap", &{S}::lowerMap<{G}::ArcMap<{T}>>, "Set the lower bounds on the arcs.")
  .def("upperMap", &{S}::upperMap<{G}::ArcMap<{T}>>, "Set the upper bounds (capacities) on the arcs.")
  .def("costMap", &{S}::costMap<{G}::ArcMap<{T}>>, "Set the costs of the arcs.")
  .def("supplyMap", &{S}::supplyMap<{G}::NodeMap<{T}>>, "Set the supply values of the nodes.")
  // running and resetting
  .def("run", &{S}::run, "Run the algorithm.")
  .def("resetParams", &{S}::resetParams, "Reset all the parameters that have been given before.")
  .def("reset", &{S}::reset, "Reset the internal data structures and all the parameters that have been given before.")
  // query results
  .def("totalCost", &{S}::totalCost<double>, "Return the total cost of the found flow.")
  .def("flow", &{S}::flow, "Return the flow on the given arc.")
  .def("potential", &{S}::potential, "Return the flow map (the primal solution).")
  .def("flowMap", &{S}::flowMap<{G}::ArcMap<{T}>>, "Return the potential (dual value) of the given node.")
  .def("potentialMap", &{S}::potentialMap<{G}::NodeMap<{T}>>, "Return the potential map (the dual solution).");

py::enum_<{S}::ProblemType>({V}, "ProblemType")
  .value("INFEASIBLE", {S}::ProblemType::INFEASIBLE)
  .value("OPTIMAL", {S}::ProblemType::OPTIMAL)
  .value("UNBOUNDED", {S}::ProblemType::UNBOUNDED);

py::enum_<{S}::Method>({V}, "Method")
  .value("PUSH", {S}::Method::PUSH)
  .value("AUGMENT", {S}::Method::AUGMENT)
  .value("PARTIAL_AUGMENT", {S}::Method::PARTIAL_AUGMENT);
