
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#define STRING(s) #s
#define CONCAT(a,b) a##b

namespace py = pybind11;

void list_digraph(py::module_ &);
void smart_digraph(py::module_ &);

PYBIND11_MODULE(api{S}, m) {{

  m.doc() = "Bindings to lemon graph algorithms with {S} bit integers.";

  list_digraph(m);
  smart_digraph(m);

}}

