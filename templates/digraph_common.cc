
graph
  .def("id", py::overload_cast<{G}::Node>(&{G}::id), "The ID of the node.")
  .def("id", py::overload_cast<{G}::Arc>(&{G}::id), "The ID of the arc.")
  .def("nodeFromId", &{G}::nodeFromId, "The node with the given ID.")
  .def("arcFromId", &{G}::arcFromId, "The arc with the given ID.")
  .def("oppositeNode", &{G}::oppositeNode, "The opposite node on the arc.")
  .def("source", &{G}::source, "The source node of the arc.")
  .def("target", &{G}::target, "The target node of the arc.");


py::class_<{G}::Node>(graph, "Node");
py::class_<{G}::Arc>(graph, "Arc");


py::class_<{G}::NodeMap<{T}>>(graph, "NodeMap")
  .def(py::init<const {G} &>())
  .def("set", &{G}::NodeMap<{T}>::set)
  .def("__setitem__", &{G}::NodeMap<{T}>::set)
  .def("get", py::overload_cast<const {G}::Node &>(&{G}::NodeMap<{T}>::operator[], py::const_))
  .def("__getitem__", py::overload_cast<const {G}::Node &>(&{G}::NodeMap<{T}>::operator[], py::const_));


py::class_<{G}::ArcMap<{T}>>(graph, "ArcMap")
  .def(py::init<const {G} &>())
  .def("set", &{G}::ArcMap<{T}>::set)
  .def("__setitem__", &{G}::ArcMap<{T}>::set)
  .def("get", py::overload_cast<const {G}::Arc &>(&{G}::ArcMap<{T}>::operator[], py::const_))
  .def("__getitem__", py::overload_cast<const {G}::Arc &>(&{G}::ArcMap<{T}>::operator[], py::const_));
