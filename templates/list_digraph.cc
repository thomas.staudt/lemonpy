
py::class_<ListDigraph> graph(m, "ListDigraph");

graph
  .def(py::init<>())
  .def("addNode", &ListDigraph::addNode, "Add a new node to the digraph.")
  .def("addArc", &ListDigraph::addArc, "Add a new arc to the digraph.")
  .def("erase", py::overload_cast<ListDigraph::Node>(&ListDigraph::erase), "Erase a node from the digraph.")
  .def("erase", py::overload_cast<ListDigraph::Arc>(&ListDigraph::erase), "Erase an arc from the digraph.")
  .def("valid", py::overload_cast<ListDigraph::Node>(&ListDigraph::valid, py::const_), "Node validity check.")
  .def("valid", py::overload_cast<ListDigraph::Arc>(&ListDigraph::valid, py::const_), "Arc validity check.")
  .def("changeTarget", &ListDigraph::changeTarget, "Change the target node of an arc.")
  .def("changeSource", &ListDigraph::changeSource, "Change the source node of an arc.")
  .def("reverseArc", &ListDigraph::reverseArc, "Reverse the direction of an arc.")
  .def("contract", &ListDigraph::contract, "Contract two nodes.")
  .def("split", py::overload_cast<ListDigraph::Node, bool>(&ListDigraph::split), "Split a node.")
  .def("split", py::overload_cast<ListDigraph::Arc>(&ListDigraph::split), "Split an arc.")
  .def("clear", &ListDigraph::clear, "Clear the digraph.")
  .def("reserveNode", &ListDigraph::reserveNode, "Reserve memory for nodes.")
  .def("reserveArc", &ListDigraph::reserveArc, "Reserve memory for arcs.");
